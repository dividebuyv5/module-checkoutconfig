<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Helper;

use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Constants\XmlFilePaths;
use Dividebuy\Common\Utility\CartHelper;
use Dividebuy\Common\Utility\SessionHelper;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
  private SessionHelper $sessionHelper;
  private StoreConfigHelper $storeConfigHelper;
  private CartHelper $cartHelper;

  public function __construct(
      Context $context,
      SessionHelper $sessionHelper,
      StoreConfigHelper $storeConfigHelper,
      CartHelper $cartHelper
  ) {
    $this->sessionHelper = $sessionHelper;
    $this->storeConfigHelper = $storeConfigHelper;
    $this->cartHelper = $cartHelper;

    parent::__construct($context);
  }

  /**
   * Retrieves the store Id.
   *
   * @return mixed
   */
  public function getStoreId()
  {
    return $this->storeConfigHelper->getStoreId();
  }

  /**
   * Retrieves the button css value from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getButtonCss($storeId = null)
  {
    return $this->storeConfigHelper->getButtonCss($storeId);
  }

  /**
   * Retrieves couriers from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getCouriers($storeId = null)
  {
    return $this->storeConfigHelper->getValue(
        XmlFilePaths::XML_PATH_DIVIDEBUY_COURIERS,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieves prefix label from configuration.
   *
   * @param  null  $storeId
   *
   * @return mixed
   */
  public function getPrefixLabel($storeId = null)
  {
    return $this->storeConfigHelper->getPrefixLabel($storeId);
  }

  /**
   * Retrieves prefix css from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getPrefixCss($storeId = null)
  {
    return $this->storeConfigHelper->getPrefixCss($storeId);
  }

  /**
   * Retrieves suffix label from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getSuffixLabel($storeId = null)
  {
    return $this->storeConfigHelper->getSuffixLabel($storeId);
  }

  /**
   * Retrieves suffix label from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getSuffixCss($storeId = null)
  {
    return $this->storeConfigHelper->getSuffixCss($storeId);
  }

  /**
   * Retrieves cart position from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getCartPosition($storeId = null)
  {
    return $this->storeConfigHelper->getCartPosition($storeId);
  }

  /**
   * Retrieves cart status from configuration.
   *
   * @param  null  $storeId
   *
   * @return bool
   */
  public function getCartStatus($storeId = null)
  {
    return $this->storeConfigHelper->getCartStatus($storeId);
  }

  /**
   * Retrieves button image url from configuration.
   *
   * @throws NoSuchEntityException
   */
  public function getButtonImageUrl(): string
  {
    return $this->storeConfigHelper->getButtonImageUrl();
  }

  /**
   * Retrieves button hover image url from configuration.
   *
   * @return mixed
   *
   * @throws NoSuchEntityException
   */
  public function getButtonHoverImageUrl()
  {
    return $this->storeConfigHelper->getButtonHoverImageUrl();
  }

  /**
   * Get current url for store.
   *
   * @param  bool|string  $fromStore  Include/Exclude from_store parameter from URL
   *
   * @return mixed
   */
  public function getStoreUrl($fromStore = true)
  {
    return $this->storeConfigHelper->getStoreUrl($fromStore);
  }

  /**
   * Get Store name.
   *
   * @return mixed
   */
  public function getStoreName()
  {
    return $this->storeConfigHelper->getStoreName();
    //return $this->_storeManager->getStore()->getName();
  }

  /**
   * Get base URL.
   *
   * @param  string  $scope
   *
   * @return mixed
   *
   * @throws NoSuchEntityException
   */
  public function getBaseUrl($scope = null)
  {
    return $this->storeConfigHelper->getBaseUrl($scope);
  }

  /**
   * Get list of postcodes in which DivideBuy shipment is not available.
   * Checks whether the given postcode is deliverable or not.
   *
   * @param  mixed  $zipcode
   *
   * @return bool
   */
  public function getDividebuyPostcodes($zipcode): bool
  {
    return $this->storeConfigHelper->isPostCodeDeliverable($zipcode);
  }

  /**
   * Displaying error message if DivideBuy shipment is not available for particular postcodes.
   */
  public function showPostcodeMsg(): string
  {
    return DivideBuy::NON_DELIVERABLE_POSTCODE_MSG;
  }

  /**
   * Retrieves minimum order amount from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getMinOrderAmount($storeId = null)
  {
    return $this->storeConfigHelper->getMinOrderAmount($storeId);
  }

  /**
   * Used to load product by id.
   *
   * @param  mixed  $id
   *
   * @return mixed
   */
  public function loadProduct($id)
  {
    return $this->cartHelper->loadProduct($id);
  }

  /**
   * Used to load quote by id.
   *
   * @param  mixed  $id
   *
   * @return mixed
   */
  public function loadQuote($id)
  {
    return $this->cartHelper->loadQuote($id);
  }

  /**
   * Used to get array of cart items with the count of dividebuy and non-dividebuy product.
   *
   * @param  null  $quoteId
   *
   * @return array|string
   */
  public function getDividebuyItemArray($quoteId = null)
  {
    return $this->cartHelper->getItemArray($quoteId);
  }

  /**
   * Used to add products in to the cart which were removed from cart and added to the checkout session.
   */
  public function addSessionProducts()
  {
    $this->sessionHelper->addSessionProducts();
  }

  /**
   * Returns the flag for checkout with coupon code.
   *
   * @return mixed
   */
  public function getCheckoputWithCouponCodeFlag()
  {
    return $this->storeConfigHelper->getCheckoutWithCouponCodeFlag();
  }

  /**
   * Used to check whether current ip is allowed to see Dividebuy functionality or not.
   *
   * @param  int  $storeId
   *
   * @return bool
   */
  public function isIPAllowed($storeId = null): bool
  {
    return $this->storeConfigHelper->isIpAllowed($storeId);
  }

  /**
   * Function to see if a string is a UK postcode or not. The postcode is also
   * formatted so it contains no strings. Full or partial postcodes can be used.
   *
   * @param  string  $toCheck
   *
   * @return bool
   */
  public function postCodeCheck($toCheck): bool
  {
    // Permitted letters depend upon their position in the postcode.
    $alpha1 = '[abcdefghijklmnoprstuwyz]';                          // Character 1
    $alpha2 = '[abcdefghklmnopqrstuvwxy]';                          // Character 2
    $alpha3 = '[abcdefghjkstuw]';                                   // Character 3
    $alpha4 = '[abehmnprvwxy]';                                     // Character 4
    $alpha5 = '[abdefghjlnpqrstuwxyz]';                             // Character 5
      $pcexp = [];
    // Expression for postcodes: AN NAA, ANN NAA, AAN NAA, and AANN NAA with a space
    // Or AN, ANN, AAN, AANN with no whitespace
    $pcexp[0] = '^('.$alpha1.'{1}'.$alpha2.'{0,1}[0-9]{1,2})([[:space:]]{0,})([0-9]{1}'.$alpha5.'{2})?$';

    // Expression for postcodes: ANA NAA
    // Or ANA with no whitespace
    $pcexp[1] = '^('.$alpha1.'{1}[0-9]{1}'.$alpha3.'{1})([[:space:]]{0,})([0-9]{1}'.$alpha5.'{2})?$';

    // Expression for postcodes: AANA NAA
    // Or AANA With no whitespace
    $pcexp[2] = '^('.$alpha1.'{1}'.$alpha2.'[0-9]{1}'.$alpha4.')([[:space:]]{0,})([0-9]{1}'.$alpha5.'{2})?$';

    // Exception for the special postcode GIR 0AA
    // Or just GIR
    $pcexp[3] = '^(gir)([[:space:]]{0,})?(0aa)?$';

    // Standard BFPO numbers
    $pcexp[4] = '^(bfpo)([[:space:]]{0,})([0-9]{1,4})$';

    // c/o BFPO numbers
    $pcexp[5] = '^(bfpo)([[:space:]]{0,})(c\/o([[:space:]]{0,})[0-9]{1,3})$';

    // Overseas Territories
    $pcexp[6] = '^([a-z]{4})([[:space:]]{0,})(1zz)$';

    // Anquilla
    $pcexp[7] = '^(ai\-2640)$';

    // Load up the string to check, converting into lowercase
    $postcode = strtolower($toCheck);

    // Assume we are not going to find a valid postcode
    $valid = false;

    // Check the string against the six types of postcodes
    foreach ($pcexp as $regexp) {
      if (preg_match('/'.$regexp.'/i', $postcode, $matches)) {
        // Load new postcode back into the form element
        $postcode = strtoupper($matches[1]);
        if (isset($matches[3])) {
          $postcode .= ' '.strtoupper($matches[3]);
        }

        // Take account of the special BFPO c/o format
        $postcode = preg_replace('/C\/O/', 'c/o ', $postcode);

        // Remember that we have found that the code is valid and break from loop
        $valid = true;

        break;
      }
    }

    return $valid;
  }
}
