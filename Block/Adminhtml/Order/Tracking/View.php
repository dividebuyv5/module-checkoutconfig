<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Block\Adminhtml\Order\Tracking;

use Dividebuy\CheckoutConfig\Block\Adminhtml\Order\Tracking;
use Dividebuy\Common\Registry;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Phrase;
use Magento\Shipping\Block\Adminhtml\Order\Tracking\View as MagentoOrderView;
use Magento\Shipping\Model\CarrierFactory;
use Magento\Shipping\Model\Config;

class View extends Tracking
{
  /**
   * @var CarrierFactory
   */
  protected CarrierFactory $carrierFactory;

  public function __construct(
      Context $context,
      Config $shippingConfig,
      Registry $registry,
      CarrierFactory $carrierFactory,
      array $data = []
  ) {
    parent::__construct($context, $shippingConfig, $registry, $data);

    $this->carrierFactory = $carrierFactory;
  }

  /**
   * Used to get carrier title.
   *
   * @param  string  $code
   *
   * @return bool|Phrase|string
   */
  public function getCarrierTitle(string $code)
  {
    $carrier = $this->carrierFactory->create($code);

    if ($carrier) {
      return $carrier->getConfigData('title');
    }
    if ($code === 'dividebuy_custom') {
      return __('Dividebuy Custom');
    }

    return __('Custom Value');
  }

  /**
   * Retrieve carriers.
   *
   * @return array
   */
  public function getCarriers(): array
  {
    $carriers = [];
    $carrierInstances = $this->_getCarriersInstances();
    $carriers['custom'] = __('Custom Value');
    $carriers['dividebuy_custom'] = 'DivideBuy Custom';

    foreach ($carrierInstances as $code => $carrier) {
      if ($carrier->isTrackingAvailable()) {
        $carriers[$code] = $carrier->getConfigData('title');
      }
    }

    return $carriers;
  }

  /**
   * Returns HTML.
   *
   * @return string|mixed
   */
  protected function _toHtml()
  {
    $this->setModuleName($this->extractModuleName(MagentoOrderView::class));

    return parent::_toHtml();
  }
}
