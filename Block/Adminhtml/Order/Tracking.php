<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Block\Adminhtml\Order;

use Dividebuy\Common\Registry;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Button;
use Magento\Sales\Model\Order\Shipment;
use Magento\Shipping\Model\Config;

class Tracking extends Template
{
  protected Registry $coreRegistry;

  protected Config $shippingConfig;

  public function __construct(Context $context, Config $shippingConfig, Registry $registry, array $data = [])
  {
    $this->shippingConfig = $shippingConfig;
    $this->coreRegistry = $registry;

    parent::__construct($context, $data);
  }

  /**
   * Retrieve shipment model instance.
   *
   * @return Shipment
   */
  public function getShipment()
  {
    return $this->coreRegistry->registry('current_shipment');
  }

  /**
   * @return array
   */
  public function getCarriers()
  {
    $carrierInstances = $this->_getCarriersInstances();

    $carriers = [];
    $carriers['custom'] = __('Custom Value');
    $carriers['dividebuy_custom'] = 'DivideBuy Custom';

    foreach ($carrierInstances as $code => $carrier) {
      if ($carrier->isTrackingAvailable()) {
        $carriers[$code] = $carrier->getConfigData('title');
      }
    }

    return $carriers;
  }

  /**
   * Prepares layout of block.
   */
  protected function _prepareLayout()
  {
    $this->addChild(
        'add_button',
        Button::class,
        ['label' => __('Add Tracking Number'), 'class' => '', 'onclick' => 'trackingControl.add()']
    );
  }

  protected function _getCarriersInstances()
  {
    $storeId = $this->getShipment() ? $this->getShipment()->getStoreId() : null;

    return $this->shippingConfig->getAllCarriers($storeId);
  }
}
