<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Block;

use Dividebuy\Common\CheckoutSession;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Registry;
use Dividebuy\Common\Utility\CartHelper;
use Dividebuy\Common\Utility\ProductHelper;
use Dividebuy\Common\Utility\SessionHelper;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Magento\Catalog\Helper\Image;
use Magento\Checkout\Model\Cart as MagentoCart;
use Magento\Checkout\Model\Session;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Cart extends Template
{
  private Image $imageHelper;

  private Data $priceHelper;

  private MagentoCart $cartModel;

  private StoreConfigHelper $storeConfig;

  private Registry $registry;

  private ApiHelper $apiHelper;

  private CartHelper $cartHelper;
  private ProductHelper $productHelper;
  private SessionHelper $sessionHelper;

  /**
   * Contains the details which are going be used on Cart page.
   */
  private array $linkData = [];

  public function __construct(
      Context $context,
      Image $imageHelper,
      Data $priceHelper,
      MagentoCart $cartModel,
      ApiHelper $apiHelper,
      Registry $registry,
      CartHelper $cartHelper,
      ProductHelper $productHelper,
      SessionHelper $sessionHelper,
      array $data = []
  ) {
    $this->imageHelper = $imageHelper;
    $this->priceHelper = $priceHelper;
    $this->cartModel = $cartModel;
    $this->apiHelper = $apiHelper;
    $this->registry = $registry;
    $this->cartHelper = $cartHelper;
    $this->productHelper = $productHelper;
    $this->sessionHelper = $sessionHelper;
    $this->storeConfig = $productHelper->getStoreConfig();

    parent::__construct($context, $data);
  }

  public function getProductId()
  {
    $product = $this->registry->registry('current_product');
    return $product ? $product->getId() : null;
  }

  /**
   * Returns the flag for in store collection logic.
   *
   * @return mixed
   */
  public function getAutoCheckoutStatus()
  {
    return $this->storeConfig->getAutoCheckoutStatus();
  }

  /**
   * Return an instance of \Magento\Checkout\Model\Session.
   *
   * @return Data
   */
  public function getPriceHelper(): Data
  {
    return $this->priceHelper;
  }

  /**
   * Used to check whether the cart contains DivideBuy Product or not.
   */
  public function checkDivideBuy(): bool
  {
    return (bool) $this->storeConfig->getCartStatus();
  }

  /**
   * Used to load quote by id.
   *
   * @param  mixed  $id
   *
   * @return mixed
   */
  public function loadQuote($id)
  {
    return $this->cartHelper->loadQuote($id);
  }

  /**
   * Used to get the details which are going be used on Cart page.
   *
   * @throws NoSuchEntityException
   */
  public function getCartLinkData(): array
  {
    $storeId = $this->storeConfig->getStoreId();
    $this->linkData['status'] = $this->storeConfig->getCartStatus($storeId);
    if ($this->linkData['status'] || $this->getUrl('checkout', ['secure' => true])) {
      $this->linkData['button_image'] = $this->storeConfig->getButtonImageUrl();
      $this->linkData['position'] = $this->storeConfig->getCartPosition($storeId);
      $this->linkData['button_hover_image'] = $this->storeConfig->getButtonHoverImageUrl();
      $this->linkData['custom_css'] = $this->storeConfig->getButtonCss($storeId);
      $this->linkData['prefix_label'] = $this->storeConfig->getPrefixLabel($storeId);
      $this->linkData['prefix_css'] = $this->storeConfig->getPrefixCss($storeId);
      $this->linkData['suffix_label'] = $this->storeConfig->getSuffixLabel($storeId);
      $this->linkData['suffix_css'] = $this->storeConfig->getSuffixCss($storeId);
      $this->linkData['retailer_image'] = $this->storeConfig->getRetailerLogoUrl();
      $this->linkData['store_name'] = $this->storeConfig->getStoreName($storeId);
      $this->linkData['softsearch_enable'] = $this->storeConfig->isCartSoftSearchEnabled($storeId);
    }

    return $this->linkData;
  }

  /**
   * Used to get an array of shipping rates.
   */
  public function getShippingRates(): array
  {
    $address = $this->cartModel->getQuote()->getShippingAddress();

    return (array) $address->collectShippingRates()->getGroupedAllShippingRates();
  }

  /**
   * Used to get only divide buy enabled products with html from current cart in HTML.
   *
   * @return string
   */
  public function getDivideBuyContent(): string
  {
    return $this->_dividebuyProducts();
  }

  /**
   * Used to get only non dividebuy products with html from current cart.
   *
   * @return string
   */
  public function getNonDivideBuyContent(): string
  {
    $otherItems = $this->getCheckoutConfigHelper()->getOtherItems(); // self::$otherItems;

    $html = ' <ul class="datas"> ';
    foreach ($otherItems as $item) {
      $_product = $item->getProduct();
      $productQty = $item->getQty();
      $productPrice = $item->getPriceInclTax();
      $productTotal = $productQty * $productPrice;
      $imgurls = $this->imageHelper->init($_product, 'cart_page_product_thumbnail')->resize(150, 60)->getUrl();

      $html .= ' <li class="clearfix"> ';
      $html .= '<div class="row">';
      $html .= '<div class="product-image-1 col-sm-3 col-xs-4"><img class="img-responsive img-thumbnail" src="'.$imgurls.'" alt=""/></div>';
      $html .= '<div class="modal-content-area col-sm-7 col-xs-6">';
      $html .= '<div class="pro-name">'.$item->getName().'</div>';
      $html .= '<span class="price">'.$productQty.' X '.$this->priceHelper->currency(
          $productPrice,
          true,
          false
      ).' = '.$this->priceHelper->currency($productTotal, true, false).'</span>';
      $html .= '</div>';
      $html .= '</div>';
      $html .= '</li>';
    }
    $html .= ' </ul> ';

    return $html;
  }

  /**
   * Used to remove all non-divide buy products from the cart before placing the divide buy order.
   */
  public function removeNonDivideBuyProducts()
  {
    $nonDivideBuyProducts = $this->getNonDivideBuyProducts();
    if (isset($nonDivideBuyProducts)) {
      // Get all cart products
      $items = [];
      try {
        $items = $this->getCheckoutSession()->getQuote()->getAllVisibleItems();
      } catch (NoSuchEntityException | LocalizedException $exception) {
        $this->storeConfig->logError($exception);
      }

      $nonDivideBuyItems = [];
      $i = 0;

      // Foreach cart products, if product is non DivideBuy then storing products in session and remove from cart.

      foreach ($items as $item) {
        if (in_array($item->getItemId(), $nonDivideBuyProducts)) {
          $nonDivideBuyItems[$i]['item_id'] = $item->getItemId();
          // Removing products from cart.
          $this->cartModel->removeItem($item->getItemId())->save();
        }

        foreach ((array) $item->getOptions() as $option) {
          if ($option->getCode() === 'info_buyRequest') {
            if (in_array($item->getItemId(), $nonDivideBuyProducts)) {
              $data = $this->storeConfig->getJsonHelper()->unserialize($option->getValue());
              if (array_key_exists('super_product_config', $data)) {
                $nonDivideBuyItems[$i]['product_type'] = 'grouped';
                $nonDivideBuyItems[$i]['qty'] = $item->getQty();
              } else {
                $product = $this->cartHelper->loadProduct($option->getProductId());
                $nonDivideBuyItems[$i]['product_type'] = $product->getTypeId();
              }
              $nonDivideBuyItems[$i]['qty'] = $item->getQty();
              $nonDivideBuyItems[$i]['product_id'] = $option->getProductId();
              $nonDivideBuyItems[$i]['info_byRequest'] = $option->getValue();
            }
          }
        }
        ++$i;
      }

      // Generating core session for storing non DivideBuy products temporary.
      if ($this->getCheckoutSession()->getTemparoryCart()) {
        $existingCartSession = $this->getCheckoutSession()->getTemparoryCart();
        $newCartSession = array_merge($nonDivideBuyItems, $existingCartSession);
        $this->getCheckoutSession()->setTemparoryCart($newCartSession);
      } else {
        $this->getCheckoutSession()->setTemparoryCart($nonDivideBuyItems);
      }
    }
  }

  /**
   * Used to get all non divide buy products with their respective ids from current cart.
   *
   * @return array
   */
  public function getNonDivideBuyProducts(): array
  {
    $nonDivideBuyProducts = $this->getItemArray();
    $nonDivideBuyProducts = preg_replace(['/^\[/', '/]$/'], '', $nonDivideBuyProducts['remove_ids'] ?? '');
    $nonDivideBuyProducts = str_replace('"', '', $nonDivideBuyProducts);
    $nonDivideBuyProducts = explode(',', $nonDivideBuyProducts);

    return array_filter($nonDivideBuyProducts);
  }

  /**
   * Used to get array of cart items with the count of dividebuy and non-dividebuy product.
   *
   * @param  null  $quoteId
   *
   * @return array|string
   */
  public function getItemArray($quoteId = null)
  {
    return $this->cartHelper->getItemArray($quoteId);
  }

  /**
   * @return Session | CheckoutSession
   */
  public function getCheckoutSession()
  {
    return $this->cartHelper->getCheckoutSession();
  }

  /**
   * Used to check that current divide buy product total is greater than the min order amount or not.
   *
   * @return bool
   */
  public function checkMinOrderAmount(): bool
  {
    if ($this->getRetailerHelper()->getMinOrderAmount() > $this->getDivideBuyTotal()) {
      return false;
    }

    return true;
  }

  /**
   * Used to check that current divide buy product total is greater than the max order amount or not.
   *
   * @return bool
   */
  public function checkMaxOrderAmount(): bool
  {
    if ($this->getRetailerHelper()->getMaxOrderAmount() < $this->getDivideBuyTotal()) {
      return false;
    }
    return true;
  }

  /**
   * Used to get the grand total of all divide buy products available in the current cart.
   *
   * @return float
   */
  public function getDivideBuyTotal()
  {
    $this->cartHelper->getItemArray();
    $otherItems = $this->cartHelper->getOtherItems();

    if (!empty($otherItems)) {
      $items = $this->cartHelper->getDivideBuyItems();
      $grandTotal = 0;
      if (!empty($items)) {
        foreach ($items as $item) {
          $productTotalPrice = $item->getRowTotalInclTax();
          $productDiscount = $item->getDiscountAmount();
          $grandTotal = $grandTotal + $productTotalPrice - $productDiscount;
        }
      }
    } else {
      $grandTotal = $this->cartModel->getQuote()->getGrandTotal();
    }

    return (float) $grandTotal;
  }

  /**
   * Used to get minimum Order amount.
   *
   * @return int
   */
  public function getMinOrderAmount()
  {
    return $this->storeConfig->getMinOrderAmount();
  }

  /**
   * Returns true if checkout with coupon code is enabled.
   *
   * @return bool $flag
   *
   * @throws LocalizedException
   *
   * @throws NoSuchEntityException
   */
  public function checkoutWithCouponCode()
  {
    $couponCode = $this->getCheckoutSession()->getQuote()->getCouponCode();
    $checkoutWithCouponCodeFlag = $this->storeConfig->getCheckoutWithCouponCodeFlag();

    $flag = 1;
    if (!$checkoutWithCouponCodeFlag && !empty($couponCode) && $couponCode !== '') {
      $flag = 0;
    }

    return $flag;
  }

  /**
   * Used to get maximum Instalment numbers.
   *
   * @return int
   */
  public function getMaxInstalmentKey()
  {
    return $this->storeConfig->getMaximumAvailableInstalments();
  }

  /**
   * Function used for getting background color.
   */
  public function getBannerBackgroundColor(): string
  {
    return (string) $this->storeConfig->getBannerBackgroundColor();
  }

  /**
   * Used to create a soft searh iframe html.
   *
   * @param  null  $softSearchSrc
   *
   * @return string
   *
   * @throws LocalizedException
   * @throws NoSuchEntityException
   */
  public function getSoftSearchHtml($softSearchSrc = null): string
  {
    $splashKey = $this->getSoftSearchSplashKey();

    if ($softSearchSrc === null) {
      $softSearchSrc = $this->apiHelper->getSplashRedirectUrl(
          $this->storeConfig->getStoreId(),
          $splashKey
      );
    }

    return '<iframe tabindex="3" style="border: 0;" id="frame-dividebuy-softcredit-check" src="'.$softSearchSrc.'" width="100%" class="frame"></iframe>';
  }

  /**
   * Used to get soft search splash key from core api.
   *
   * @throws LocalizedException|NoSuchEntityException
   */
  public function getSoftSearchSplashKey(): string
  {
    $splashKeyRequest = [];
    $total = $this->getDivideBuyTotal();
    $orderTotal = number_format($total, 2) ?? number_format($this->storeConfig->getMinOrderAmount(), 2);

    $splashKeyRequest['retailer_base_url'] = rtrim($this->_storeManager->getStore()->getBaseUrl(), '/');
    $splashKeyRequest['order_total'] = $orderTotal;
    $splashKeyRequest['order_total'] = str_replace(',', '', $splashKeyRequest['order_total']);

    $splashKeyResponse = $this->apiHelper->getSdkApi()->getSoftSearchKey($splashKeyRequest);

    return $splashKeyResponse['splashKey'] ?? '';
  }

  /**
   * Function used for getting softsearch html for cms page.
   *
   * @return string
   *
   * @throws NoSuchEntityException
   */
  public function getSoftSearchPageHtml(): string
  {
    $splashKey = $this->getSoftSearchSplashKeyPage();
    $softSearchSrc = $this->apiHelper->getSplashRedirectUrl(
        $this->storeConfig->getStoreId(),
        $splashKey
    );

    return '<iframe tabindex="3" style="border: 0;" id="frame-dividebuy-softcredit-check" src="'.$softSearchSrc.'" width="100%" class="frame"></iframe>';
  }

  /**
   * Function used for getting splashkey for cms page.
   *
   * @throws NoSuchEntityException
   */
  public function getSoftSearchSplashKeyPage(): string
  {
    $splashKeyRequest = [];
    $maxValue = $this->storeConfig->getMaximumAvailableInstalmentValue();

    $splashKeyRequest['retailer_base_url'] = rtrim($this->_storeManager->getStore()->getBaseUrl(), '/');
    $splashKeyRequest['order_total'] = str_replace(',', '', $maxValue);

    $splashKeyResponse = $this->apiHelper->getSdkApi()->getSoftSearchKey($splashKeyRequest);

    return $splashKeyResponse['splashKey'] ?? '';
  }

  /**
   * Function used for getting softsearch html for product page.
   *
   * @throws NoSuchEntityException
   */
  public function getSoftSearchProductHtml(): string
  {
    $orderTotal = $this->registry->registry('current_product')->getPrice();
    $orderTotal = number_format((float) $orderTotal, 2);
    $splashKey = $this->getSoftSearchSplashKeyProduct($orderTotal);

    $softSearchSrc = $this->apiHelper->getSplashRedirectUrl(
        $this->storeConfig->getStoreId(),
        $splashKey
    );

    return '<iframe tabindex="3" style="border: 0;" id="frame-dividebuy-softcredit-check" src="'.$softSearchSrc.'" width="100%" class="frame"></iframe>';
  }

  /**
   * Function used for getting splash key for product page.
   *
   * @param  mixed  $orderTotal
   *
   * @return string
   *
   * @throws NoSuchEntityException
   */
  public function getSoftSearchSplashKeyProduct($orderTotal): string
  {
    $splashKeyRequest = [];
    $splashKeyRequest['retailer_base_url'] = rtrim($this->_storeManager->getStore()->getBaseUrl(), '/');
    $splashKeyRequest['order_total'] = $orderTotal;

    $splashKeyResponse = $this->apiHelper->getSdkApi()->getSoftSearchKey($splashKeyRequest);
    return $splashKeyResponse['splashKey'] ?? '';
  }

  /**
   * Used in view files
   *
   * @return CartHelper
   */
  public function getCheckoutConfigHelper()
  {
    return $this->cartHelper;
  }

  /**
   * Used in view files
   *
   * @return StoreConfigHelper
   */
  public function getRetailerHelper()
  {
    return $this->storeConfig;
  }

  public function getSymbol(): string
  {
    return $this->productHelper->getSymbol();
  }

  /**
   * Used to get only dividebuy enabled products with html from current cart.
   */
  protected function _dividebuyProducts(): string
  {
    $items = $this->cartHelper->getDivideBuyItems(); // self::$divideBuyItems;

    $html = ' <ul class="datas"> ';
    foreach ($items as $item) {
      $_product = $item->getProduct();
      $productQty = $item->getQty();
      $productPrice = $item->getPriceInclTax();
      $productTotal = $productQty * $productPrice;

      $imageUrls = $this->imageHelper->init($_product, 'cart_page_product_thumbnail')->resize(150, 60)->getUrl();
      $html .= ' <li class="clearfix"> ';
      $html .= ' <div class="row"> ';
      $html .= '<div class="product-image-1 col-sm-3 col-xs-4"><img class="img-responsive img-thumbnail" src="'.$imageUrls.'" alt=""/></div>';
      $html .= '<div class="modal-content-area col-sm-7 col-xs-6"><div class="pro-name">'.$item->getName().'</div><span class="price">'.$productQty.' X '.$this->priceHelper->currency(
          $productPrice,
          true,
          false
      ).' = '.$this->priceHelper->currency($productTotal, true, false).'</span></div></div>';
      $html .= ' </li> ';
    }
    $html .= ' </ul> ';

    return $html;
  }

  public function calculateInstalmentAmount($amount, $monthlyIR, $numberOfMonths, $roundingPrecision = 6) {
        return round(
            ($amount * $monthlyIR) / (1 - pow(1 + $monthlyIR, ($numberOfMonths * -1))), 
            $roundingPrecision
        );
    }

    public function convertMonthsToYears($numberOfMonths) {
        $fullYears = floor($numberOfMonths / 12);
        $results = [];

        if ((int) $fullYears !== 0) {
            $results[] = $fullYears . " years";
        }

        $remainingMonths = $numberOfMonths - ($fullYears * 12);
        if ((int) $remainingMonths !== 0) {
            $results[] = $remainingMonths . " months";
        }

        return implode(', ', $results);
    }

    public function calculateInstalmentAmountsForLoan($loanAmount, $interestRate, $monthsInAgreement) {
        // Calculate the monthly interest rate
        $loanAmount = (float)str_replace('£',"",(string)$loanAmount);
        //echo $interestRate; echo "</br>";
        $monthlyInterestRate = round(pow(($interestRate + 1) , (1 / 12)) - 1, 6);

        if($interestRate == 0)
        {
          $instalment = (float) round($loanAmount / $monthsInAgreement, 2);
            return [
            'totalAmount' => $loanAmount,
            'interestRate' => 0,
            'monthlyInterestRate' => 0,
            'numberOfMonths' => $monthsInAgreement,
            'firstInstalment' => $instalment,
            'regularMonthlyInstalment' => $instalment, 
            'totalToPay' => $loanAmount,
            'totalLoanValue' => $loanAmount - $instalment,
            'interest' => 0,
            'effectiveTerm' => $monthsInAgreement - 1,
            ];
        }

        // Establish the initial payment to be made by the customer
        // This is INTEREST FREE, and MUST be calculated in pence.
        $loanAmountInPence = $loanAmount * 100;
        $initialRemainder = fmod($loanAmountInPence, (float)$monthsInAgreement);
        $initialRegularInstalment = ($loanAmountInPence - $initialRemainder) / $monthsInAgreement;
        $firstInstalment = ($initialRegularInstalment + $initialRemainder) / 100;

        // The loan principle is the amount left after the initial payment has been made. This is the amount that is
        // actively being loaned, which will require interest
        $loanPrincipal = $loanAmount - $firstInstalment;

        // Establish the monthly rate for this new loan principal amount, to be paid across one fewer instalments than
        // defined, as the first instalment has already been paid. 
        // This is required as we need to calculate interest on the slightly lower amount of the loan principle, not
        // against the amount of the original order.

        $monthlyInstalment = round(
            ($loanPrincipal * $monthlyInterestRate) / (1 - pow(1 + $monthlyInterestRate, (($monthsInAgreement - 1) * -1))), 6
        );

        // Calculate the total to be paid, and the interest that includes
        $totalToPay = ($monthlyInstalment * ($monthsInAgreement - 1)) + $firstInstalment;
        $interestToPay = $totalToPay - $loanAmount;
        $regularMonthlyInstalment = (float) bcdiv((string)$monthlyInstalment, "1", 2);
        $toBeAddedToFinalInstalment = $totalToPay - $firstInstalment - (($monthsInAgreement - 1) * $regularMonthlyInstalment);
        $finalInstalment = $regularMonthlyInstalment + $toBeAddedToFinalInstalment;

        return [
            'totalAmount' => $loanAmount,
            'interestRate' => $interestRate,
            'monthlyInterestRate' => $monthlyInterestRate,
            'numberOfMonths' => $monthsInAgreement,
            'firstInstalment' => $firstInstalment,
            'regularMonthlyInstalment' => $regularMonthlyInstalment, 
            'totalToPay' => round($totalToPay, 2),
            'totalLoanValue' => round($totalToPay - $firstInstalment, 2),
            'interest' => round($interestToPay, 2),
            'effectiveTerm' => $monthsInAgreement - 1,
            'finalInstalment' => $finalInstalment,
        ];
    }

    public function retailerHasInterestBearingInstalment() {
        $interestBearingInstalmentFound = false;
        $instalments = $this->sessionHelper->getStoreConfigHelper()->getInstallments();
        $instalments_ibc =$this->sessionHelper->getStoreConfigHelper()->getIbcInstallments();

        foreach (array_merge($instalments, $instalments_ibc) as $instalment) {
            if(array_key_exists('interest_rate', $instalment)){
                if ((float) $instalment['interest_rate'] > 0) {
                    $interestBearingInstalmentFound = true;
                }
            }
        }

        return $interestBearingInstalmentFound;
    }

    public function getIbcInstallments()
    {
      return $this->sessionHelper->getStoreConfigHelper()->getIbcInstallments();
    }

    public function GetAprRate(){
      $interest_rate = $this->sessionHelper->getStoreConfigHelper()->GetAprRates();
      return $interest_rate;
    }

    public function GetAvgIbcTermLen(){
      $avg_ibc_term_len = $this->sessionHelper->getStoreConfigHelper()->GetAvgIbcTermLength();
      return $avg_ibc_term_len;
    }

    public function GetAvgIbcOrderVal(){
      $avg_ibc_order_val = $this->sessionHelper->getStoreConfigHelper()->GetAvgIbcOrderValue();
      return $avg_ibc_order_val;
    }

    public function getTermsAndConditionsUrl(){
      $terms_condition_url = $this->sessionHelper->getStoreConfigHelper()->getTermsAndConditionsUrl();
      return $terms_condition_url;
    }

    public function getRepresentativeAprExampleHtml($amount, $interestRate, $numberOfMonths){
      if (! $this->retailerHasInterestBearingInstalment()) {
          return "0% APR representative example. Credit subject to status and minimum order value.";
      }

        $repAprExample = $this->calculateInstalmentAmountsForLoan(
            $amount, 
            $interestRate / 100, 
            $numberOfMonths
        );

        return "<strong>Representative Example:</strong> 
            Cash price of goods is £" . number_format($amount, 0, '.', ',') . ". 
            Initial payment of £".$repAprExample['firstInstalment'] .". 
            Total amount of credit is £" . $amount - $repAprExample['firstInstalment'] . ". 
            The total amount payable is £" . number_format($repAprExample['totalToPay'], 2, '.', ',') . ", 
            which includes £" . number_format($repAprExample['interest'], 2, '.', ',') . " 
            in interest at " . number_format((float) $interestRate, 2, '.', ',') . "% fixed. 
            The repayments comprise " . $repAprExample['numberOfMonths'] - 2 . " 
            monthly payments of £" . $repAprExample['regularMonthlyInstalment'] . " and 
            a final payment of £" . number_format($repAprExample['finalInstalment'], 2, '.', ',') . " 
            at a representative " . number_format((float) $interestRate, 2, '.', ',') . "% APR.";
    }

    /**
    * Used to check which promotion text option is selected.
    */
    public function getProductPromotionText()
    {
      return $this->storeConfig->getProductPromotionText();
    }

    /**
    * Used to check selected option for tooltip amount
    */
    public function getFinanceCalculation()
    {
      return $this->storeConfig->getFinanceCalculation();
    }
}
