<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Controller\Index;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\CartHelper;
use Dividebuy\Common\Utility\ResponseHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class GetAvailableInstallments extends AbstractActionController
{
  use CsrfAwareActionTrait;

  protected ResponseHelper $responseHelper;

  protected CartHelper $cartHelper;

  public function __construct(Context $context, CartHelper $cartHelper, ResponseHelper $responseHelper)
  {
    $this->responseHelper = $responseHelper;
    $this->cartHelper = $cartHelper;

    parent::__construct($context);
  }

  /**
   * Redirect to the particular action according to the products in cart.
   *
   * @return ResponseInterface|ResultInterface|void
   */
  public function execute()
  {
    return $this->responseHelper->sendJsonResponse(['data' => $this->cartHelper->getAvailableInstalments()]);
  }
}
