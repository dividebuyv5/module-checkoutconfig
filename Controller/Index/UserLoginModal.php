<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Controller\Index;

use Dividebuy\CheckoutConfig\Block\Cart as CheckoutBlock;
use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;

class UserLoginModal extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected PageFactory $_resultPageFactory;

  protected CheckoutBlock $_checkoutBlock;

  public function __construct(Context $context, PageFactory $resultPageFactory, CheckoutBlock $checkoutBlock)
  {
    $this->_resultPageFactory = $resultPageFactory;
    $this->_checkoutBlock = $checkoutBlock;

    parent::__construct($context);
  }

  /**
   * Used to load the user_login.phtml file.
   *
   * @throws LocalizedException
   */
  public function execute()
  {
    $modalBlock = $this->_checkoutBlock
        ->getLayout()
        ->createBlock('Dividebuy\CheckoutConfig\Block\Cart')
        ->setTemplate('Dividebuy_CheckoutConfig::dividebuy/cart/modal/user_login.phtml')
        ->toHtml()
        ;

    return $this->getResponse()
        ->setHeader('Content-Type', 'text/html')
        ->setBody($modalBlock)
        ;
  }
}
