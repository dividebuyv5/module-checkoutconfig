<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Controller\Index;

use Dividebuy\CheckoutConfig\Block\Cart as CheckoutBlock;
use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

class ShippingModal extends AbstractActionController
{
  use CsrfAwareActionTrait;

  protected CheckoutBlock $_checkoutBlock;

  public function __construct(Context $context, CheckoutBlock $checkoutBlock)
  {
    $this->_checkoutBlock = $checkoutBlock;

    parent::__construct($context);
  }

  /**
   * Used to load the shipping_information.phtml file.
   *
   * @throws LocalizedException
   */
  public function execute()
  {
    $this->_checkoutBlock->removeNonDivideBuyProducts();

    $modalBlock = $this->_checkoutBlock
        ->getLayout()
        ->createBlock('Dividebuy\CheckoutConfig\Block\Cart')
        ->setTemplate('Dividebuy_CheckoutConfig::dividebuy/cart/modal/shipping_information.phtml')
        ->toHtml();

    return $this->getResponse()
        ->setHeader('Content-Type', 'text/html')
        ->setBody($modalBlock);
  }
}
