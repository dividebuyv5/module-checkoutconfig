<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Controller\Index;

use Dividebuy\CheckoutConfig\Block\Cart as CheckoutBlock;
use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class GetSoftSearchHtml extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  private ResponseHelper $responseHelper;

  private CheckoutBlock $checkoutBlock;

  private ApiHelper $apiHelper;

  private StoreConfigHelper $storeConfigHelper;

  public function __construct(
      Context $context,
      CheckoutBlock $checkoutBlock,
      ResponseHelper $responseHelper,
      ApiHelper $apiHelper,
      StoreConfigHelper $storeConfigHelper
  ) {
    $this->checkoutBlock = $checkoutBlock;
    $this->responseHelper = $responseHelper;
    $this->apiHelper = $apiHelper;
    $this->storeConfigHelper = $storeConfigHelper;

    parent::__construct($context);
  }

  /**
   * Redirect to the particular action according to the products in cart.
   *
   * @return ResponseInterface|ResultInterface
   *
   * @throws LocalizedException
   * @throws NoSuchEntityException
   */
  public function execute()
  {

    $urlDomain = $this->apiHelper->getSoftSearchDomain($this->storeConfigHelper->getStoreId());

    $softSearchSrc = $urlDomain . '?splashKey=' . $this->checkoutBlock->getSoftSearchSplashKey() . '&customerEmail=' . $this->getRequest()->getParam('customerEmail') . '&redirectUrl=' . $this->getRequest()->getParam('redirectUrl');

    return $this->responseHelper->sendJsonResponse(['data' => $this->checkoutBlock->getSoftSearchHtml($softSearchSrc)]);
  }
}
