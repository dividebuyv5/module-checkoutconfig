<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Controller\Index;

use Dividebuy\CheckoutConfig\Block\Cart as CheckoutBlock;
use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Cache\TypeListInterface as CacheTypeListInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;

class ContinueToCheckout extends AbstractActionController
{
  use CsrfAwareActionTrait;

  protected PageFactory $_resultPageFactory;

  protected CheckoutBlock $_checkoutBlock;

  protected Session $_checkoutSession;

  protected CustomerSession $_customerSession;

  protected CacheTypeListInterface $cache;

  public function __construct(
      Context $context,
      PageFactory $resultPageFactory,
      CheckoutSession $checkoutSession,
      CheckoutBlock $checkoutBlock,
      CacheTypeListInterface $cache,
      CustomerSession $customerSession
  ) {
    $this->_resultPageFactory = $resultPageFactory;
    $this->_checkoutSession = $checkoutSession;
    $this->_customerSession = $customerSession;
    $this->_checkoutBlock = $checkoutBlock;
    $this->cache = $cache;

    parent::__construct($context);
  }

  /**
   * Redirect to the particular action according to the products in cart.
   *
   * @return Redirect|ResponseInterface|ResultInterface
   *
   * @throws NoSuchEntityException
   *
   * @throws LocalizedException
   */
  public function execute()
  {
    $this->cache->cleanType('full_page');
    $this->cache->cleanType('block_html');
    // Storing value of checkout session and customer session.
    $sessionGuest = $this->_checkoutSession->getguest();
    $sessionCustomer = $this->_customerSession;

    $this->setCurrentShippingData();

    // Get count of dividebuy and non-dividebuy products in cart with use of Checkout Module
    $checkCart = $this->_checkoutBlock->getItemArray();
    $nonDividebuyProductCount = $checkCart['nodividebuy'];
    $userLoggedIn = 'false';
    // Check if current session is of guest
    if ($sessionGuest || $sessionCustomer->isLoggedIn() || $this->_checkoutSession->getCheckoutPage()) {
      // Check for non-dividebuy products count in cart and set flag value based on it.
      if ($nonDividebuyProductCount === 0) {
        $userLoggedIn = 'true_no_mixed_products';
      } else {
        $userLoggedIn = 'true_with_mixed_products';
      }
    }

    $resultRedirect = $this->resultRedirectFactory->create();

    switch ($userLoggedIn) {
      // If there are only DivideBuy products in cart, display shipping information.
      case 'true_no_mixed_products':
        $resultRedirect->setPath('checkoutconfig/index/shippingmodal');

        break;
      // If there are mixed products in cart, display seperated products in modal with grand total.
      case 'true_with_mixed_products':
        $resultRedirect->setPath('checkoutconfig/index/cartmodal');

        break;
      // If user is neither logged in nor guest session, display login form.
      case 'false':
        $resultRedirect->setPath('checkoutconfig/index/userloginmodal');

        break;
    }

    return $resultRedirect;
  }

  /**
   * Sets the shipping method and amount in current checkout sesion.
   *
   * @throws LocalizedException
   * @throws NoSuchEntityException
   */
  protected function setCurrentShippingData()
  {
    $this->_checkoutSession->unsShipping();
    $quote = $this->_checkoutSession->getQuote();
    $quoteShipMethod = $quote->getShippingAddress()->getShippingMethod();
    $quoteShipMethodPrice = $quote->getShippingAddress()->getShippingAmount();
    $shipping = [$quoteShipMethod, $quoteShipMethodPrice];
    $this->_checkoutSession->setShipping($shipping);
  }
}
