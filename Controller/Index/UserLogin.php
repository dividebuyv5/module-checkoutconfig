<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Controller\Index;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Exception;
use Magento\Checkout\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\View\Result\PageFactory;

class UserLogin extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected PageFactory $_resultPageFactory;

  /**
   * @var Session
   */
  protected $_customerSession;

  protected AccountManagementInterface $_customerAccountManagement;

  public function __construct(
      Context $context,
      PageFactory $resultPageFactory,
      CustomerSession $customerSession,
      AccountManagementInterface $customerAccountManagement
  ) {
    $this->_resultPageFactory = $resultPageFactory;
    $this->_customerSession = $customerSession;
    $this->_customerAccountManagement = $customerAccountManagement;

    parent::__construct($context);
  }

  /**
   * Check the user credentials and generates session based on it.
   *
   * @return mixed
   */
  public function execute()
  {
    // Getting value of email and password enter by user.
    $email = htmlspecialchars((string) $this->getRequest()->getParam('userEmail'), ENT_QUOTES);
    $password = htmlspecialchars((string) $this->getRequest()->getParam('userPassword'), ENT_QUOTES);

    // Check if entered credentials are correct.
    try {
      $customer = $this->_customerAccountManagement->authenticate($email, $password);
      $this->_customerSession->setCustomerDataAsLoggedIn($customer);
      $this->_customerSession->regenerateId();

      $resultRedirect = $this->resultRedirectFactory->create();
      $resultRedirect->setPath('checkoutconfig/index/continuetocheckout');

      return $resultRedirect;
    } catch (Exception $ex) {
      return false;
    }
  }
}
