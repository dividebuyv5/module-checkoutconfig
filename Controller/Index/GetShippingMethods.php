<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Controller\Index;

use Dividebuy\CheckoutConfig\Block\Cart as CheckoutBlock;
use Dividebuy\CheckoutConfig\Helper\Data as CheckoutHelper;
use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

class GetShippingMethods extends AbstractActionController
{
  use CsrfAwareActionTrait;

  protected Cart $_cartModel;

  protected CheckoutBlock $_checkoutBlock;

  protected CheckoutHelper $_checkoutHelper;

  public function __construct(
      CheckoutHelper $checkoutHelper,
      Context $context,
      Cart $cartModel,
      CheckoutBlock $checkoutBlock
  ) {
    $this->_cartModel = $cartModel;
    $this->_checkoutBlock = $checkoutBlock;
    $this->_checkoutHelper = $checkoutHelper;

    parent::__construct($context);
  }

  /**
   * Used to load the shipping_methods.phtml file.
   *
   * @throws LocalizedException
   */
  public function execute()
  {
    $zipcode = htmlspecialchars((string) $this->getRequest()->getParam('user_postcode'), ENT_QUOTES);

    // Checking if DivideBuy ships has allowed to ship items for entered postcode by user.
    $zipcodeVerification = $this->_checkoutHelper->getDividebuyPostcodes($zipcode);
    if (!$zipcodeVerification) {
      $modalBlock = "<span style='color: red'>".$this->_checkoutHelper->showPostcodeMsg().'</span>';
    } elseif (!$this->_checkoutHelper->postCodeCheck($zipcode)) {
      $modalBlock = "<span style='color: red'>Please enter valid postcode!</span>";
    } else {
      $this->_updateShippingData();
      $modalBlock = $this->_checkoutBlock
          ->getLayout()
          ->createBlock('Dividebuy\CheckoutConfig\Block\Cart')
          ->setTemplate('Dividebuy_CheckoutConfig::dividebuy/cart/modal/shipping_methods.phtml')
          ->toHtml();
    }

    return $this->getResponse()
        ->setHeader('Content-Type', 'text/html')
        ->setBody($modalBlock);
  }

  /**
   * Sets zipcode and country code for current shipping addresses.
   */
  protected function _updateShippingData()
  {
    $zipcode = htmlspecialchars((string) $this->getRequest()->getParam('user_postcode'), ENT_QUOTES);
    $country = 'GB';

    // Update the cart's quote.
    $address = $this->_cartModel->getQuote()->getShippingAddress();
    $address->setCountryId($country)
        ->setPostcode($zipcode)
        ->setCollectShippingrates(true);
    $this->_cartModel->save();
  }
}
