<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Controller\Index;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;

class ContinueGuest extends AbstractActionController
{
  use CsrfAwareActionTrait;

  protected Session $_checkoutSession;

  public function __construct(Context $context, Session $checkoutSession)
  {
    $this->_checkoutSession = $checkoutSession;

    parent::__construct($context);
  }
  /**
   * Check the user credentials and generates session based on it.
   */
  public function execute()
  {
    $this->_checkoutSession->setguest('1');
    $resultRedirect = $this->resultRedirectFactory->create();
    $resultRedirect->setPath('checkoutconfig/index/continuetocheckout');

    return $resultRedirect;
  }
}
