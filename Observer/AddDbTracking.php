<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\LocalizedException;

class AddDbTracking extends AbstractShipmentObserver
{
  /**
   * @param  Observer  $observer
   *
   * @return void
   *
   * @throws LocalizedException
   */
  public function execute(Observer $observer)
  {
    // Skipping the operation if this shipment is a new shipment
    if ($this->customerSession->getIsNewShipment() === 1) {
      $this->customerSession->unsIsNewShipment();
    } else {
      $tracking = $observer->getEvent()->getTrack();
      $orderId = $tracking->getOrderId();
      $order = $this->orderLoader->load($orderId);
      $isDivideBuy = $this->checkDivideBuyCarrier($order);

      if ($isDivideBuy) {
        $shipment = $this->shipmentLoader->load($tracking->getParentId());
        $params = $this->getTrackingRequest($shipment);
        //@todo check response format
        $response = $this->apiHelper->getSdkApi()->trackOrder($params);

        $this->handleException($response, 'Unable to save shipment in DivideBuy');
      }
    }
  }

  /**
   * Used to get shipment tracking details.
   */
  protected function getShipmentTracking(): array
  {
    $i = 0;
    $params = $this->getRequest()->getParams();
    $shipmentTrackData = [];

    $shipmentTrackData[$i]['trackNumber'] = $params['number'];
    $shipmentTrackData[$i]['description'] = '';
    $shipmentTrackData[$i]['title'] = $params['title'];
    $shipmentTrackData[$i]['carrierCode'] = $params['carrier'];

    return $shipmentTrackData;
  }
}
