<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\LocalizedException;

class ProcessShipment extends AbstractShipmentObserver
{
  /**
   * @param  Observer  $observer
   *
   * @throws LocalizedException
   */
  public function execute(Observer $observer)
  {
    $shipment = $observer->getEvent()->getShipment();
    $isDivideBuy = $this->checkDivideBuyCarrier($shipment->getOrder());
    $shipmentTrack = $this->request->getParam('tracking');

    if ($isDivideBuy && (isset($shipmentTrack) && !empty($shipmentTrack))) {
      $params = $this->getTrackingRequest($shipment);
      $this->customerSession->setIsNewShipment(1);
      $response = $this->apiHelper->getSdkApi()->trackOrder($params);

      $this->handleException($response, 'Unable to save shipment in DivideBuy');
    }
  }
}
