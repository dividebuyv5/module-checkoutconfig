<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Observer;

use Dividebuy\Common\EventObserver;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\LocalizedException;

class DeleteDbShipment extends AbstractShipmentObserver
{
  /**
   * @param  Observer|EventObserver  $observer
   *
   * @return void
   *
   * @throws LocalizedException
   */
  public function execute(Observer $observer)
  {
    $tracking = $observer->getEvent()->getTrack();
    $orderId = $tracking->getOrderId();
    $order = $this->orderLoader->load($orderId);
    $isDivideBuy = $this->checkDivideBuyCarrier($order);

    if ($isDivideBuy) {
      $shipment = $this->shipmentLoader->load($tracking->getParentId());
      $deleteTracking = 1;
      $params = $this->getTrackingRequest($shipment, $deleteTracking, $tracking);
      $response = $this->apiHelper->getSdkApi()->trackOrder($params);

      $this->handleException($response, 'Unable to delete shipment in DivideBuy');
    }
  }
}
