<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Observer;

use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Dividebuy\Common\CustomerSession;
use Magento\Customer\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Shipment;

abstract class AbstractShipmentObserver implements ObserverInterface
{
  protected RequestInterface $request;

  protected StoreConfigHelper $configHelper;

  protected ApiHelper $apiHelper;

  protected Order $orderLoader;

  protected Shipment $shipmentLoader;

  /**
   * @var Session | CustomerSession
   */
  protected Session $customerSession;

  public function __construct(
      RequestInterface $request,
      StoreConfigHelper $configHelper,
      ApiHelper $apiHelper,
      Order $order,
      Shipment $shipment,
      Session $customerSession
  ) {
    $this->request = $request;
    $this->configHelper = $configHelper;
    $this->apiHelper = $apiHelper;
    $this->orderLoader = $order;
    $this->shipmentLoader = $shipment;
    $this->customerSession = $customerSession;
  }

  public function getConfigHelper(): StoreConfigHelper
  {
    return $this->configHelper;
  }

  public function getRequest(): RequestInterface
  {
    return $this->request;
  }

  /**
   * Check if the order payment method is DivideBuy.
   *
   * @param  $order
   *
   * @return bool
   */
  protected function checkDivideBuyCarrier($order): bool
  {
    $flag = false;
    $paymentMethod = $order->getPayment()->getMethod();
    if ($paymentMethod === DivideBuy::DIVIDEBUY_PAYMENT_CODE) {
      $flag = true;
    }

    return $flag;
  }

  /**
   * Will create a tracking request for DivideBuy.
   *
   * @param  mixed  $shipment
   * @param  int  $deleteTracking
   * @param  array  $tracking
   *
   * @return array
   */
  protected function getTrackingRequest($shipment, $deleteTracking = 0, $tracking = null): array
  {
    $params = [];
    $productDetails = [];
    $order = $shipment->getOrder();

    $storeId = $order->getStoreId();
    $params['retailerId'] = $this->getConfigHelper()->getRetailerId($storeId);

    $params['storeOrderId'] = $shipment->getOrderId();
    $params['storeToken'] = $this->getConfigHelper()->getStoreToken();
    $params['storeAuthentication'] = $this->getConfigHelper()->getAuthenticationKey();
    $params['deleteTracking'] = $deleteTracking;

    if (!$deleteTracking) {
      $params['trackingInfo'] = $this->getShipmentTracking();
    } else {
      $trackArray = [];
      $trackArray['trackNumber'] = $tracking->getTrackNumber();
      $trackArray['title'] = $tracking->getTitle();
      $trackArray['carrierCode'] = $tracking->getCarrierCode();
      $params['trackingInfo'][] = $trackArray;
    }

    //getting product Information
    $i = 0;
    foreach ($shipment->getAllItems() as $item) {
      $productDetails[$i]['sku'] = $item->getSku();
      $productDetails[$i]['qty'] = $item->getQty();
      ++$i;
    }
    $params['productDetails'] = $productDetails;

    return $params;
  }

  /**
   * Used to get shipment tracking details.
   */
  protected function getShipmentTracking(): array
  {
    $shipmentTrackData = [];
    $shipmentTrack = $this->getRequest()->getParam('tracking');

    if (isset($shipmentTrack) && !empty($shipmentTrack)) {
      $i = 0;
      foreach ($shipmentTrack as $track) {
        $shipmentTrackData[$i]['trackNumber'] = $track['number'];
        $shipmentTrackData[$i]['description'] = '';
        $shipmentTrackData[$i]['title'] = $track['title'];
        $shipmentTrackData[$i]['carrierCode'] = $track['carrier_code'];
        ++$i;
      }
    }

    return $shipmentTrackData;
  }

  /**
   * @param $response
   * @param $message
   *
   * @throws LocalizedException
   */
  protected function handleException($response, $message)
  {
    if (empty($response['status']) && !empty($response['error'])) {
      throw new LocalizedException(__($message.': '.json_encode($response['message'])));
    }
  }
}
