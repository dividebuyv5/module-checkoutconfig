<?php

declare(strict_types=1);

namespace Dividebuy\CheckoutConfig\Model;

use Dividebuy\Common\Registry;
use Magento\Catalog\Model\Product;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Store\Model\StoreManagerInterface;

class CartModel extends AbstractModel
{
  /**
   * @var CheckoutSession
   */
  protected $checkoutSession;

  protected Session $customerSession;

  protected Product $productLoader;

  public function __construct(
      Cart $checkoutSession,
      Session $customerSession,
      Product $productLoader,
      Context $context,
      Registry $registry
  ) {
    parent::__construct($context, $registry);

    $this->checkoutSession = $checkoutSession;
    $this->customerSession = $customerSession;
    $this->productLoader = $productLoader;
  }

  /**
   * Used to add product in cart.
   *
   * @param mixed   $productId
   * @param mixed $itemParams
   *
   * @throws LocalizedException
   */
  public function addProductsInCart($productId, $itemParams)
  {
    $cart = $this->checkoutSession;
    $objectManager = ObjectManager::getInstance();
    $storeId = $objectManager->get(StoreManagerInterface::class)->getStore()->getId();
    $product = $objectManager->create(Product::class)->setStoreId($storeId)->load($productId);

    $cart->addProduct($product, $itemParams);
    $session = $this->customerSession;
    $session->setCartWasUpdated(true);
    $cart->save();
  }
}
