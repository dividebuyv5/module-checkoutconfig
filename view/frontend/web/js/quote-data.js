define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'uiRegistry'
], function myscript($, ko, Component, customerData, registry, total) {
    'use strict';

    return Component.extend({

        /** @inheritdoc */
        initialize: function () {
            this._super();

            var self = this;
            var total = '';

            customerData.get('cart-data').subscribe(function (cartData) {
                total = cartData.totals;
                if(total){
                    total = total.base_shipping_incl_tax;
                }
                else{
                    total = 0.00;
                }
                localStorage.setItem('shipping_amount', total);
            }, this);
        },
    });
});


