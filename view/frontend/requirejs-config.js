var config = {
     map: {
        '*': {
            myscript: 'Dividebuy_CheckoutConfig/js/quote-data',
        }
    },
    paths: {
        jquerymin: 'Dividebuy_CheckoutConfig/js/jquery.min',
        bootstrapmin: 'Dividebuy_CheckoutConfig/js/bootstrap.min',
        dividebuy: 'Dividebuy_CheckoutConfig/js/dividebuy',
        jsPostcodes: 'Dividebuy_CheckoutConfig/js/jspostcode',
    },
    shim: {
        'jquerymin': {
            deps: ['jquery']
        },
        'dividebuy': {
            deps: ['jquery']
        },
        'jsPostcodes': {
            deps: ['jquery']
        }
    }
};